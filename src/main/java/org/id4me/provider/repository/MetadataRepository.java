package org.id4me.provider.repository;

import org.id4me.provider.model.MetadataModel;
import org.springframework.data.repository.CrudRepository;

public interface MetadataRepository extends CrudRepository<MetadataModel, Long> {
}
