package org.id4me.provider.service;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.openid.connect.sdk.federation.entities.EntityID;
import com.nimbusds.openid.connect.sdk.federation.entities.EntityStatement;
import com.nimbusds.openid.connect.sdk.federation.entities.EntityStatementClaimsSet;
import com.nimbusds.openid.connect.sdk.federation.entities.FederationMetadataType;
import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;
import net.minidev.json.parser.ParseException;
import org.id4me.provider.common.ProviderConfig;
import org.id4me.provider.repository.MetadataRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class StatementService {

    private static final Logger logger = LoggerFactory.getLogger(StatementService.class);

    private final KeyService keyService;
    private final ProviderConfig providerConfig;
    private final MetadataRepository metadataRepository;

    public StatementService(KeyService keyService, ProviderConfig providerConfig, MetadataRepository metadataRepository) {
        this.keyService = keyService;
        this.providerConfig = providerConfig;
        this.metadataRepository = metadataRepository;
    }

    public String composeSelfSignedStatement() {
        EntityStatementClaimsSet entityStatementClaimsSet = new EntityStatementClaimsSet(new EntityID(providerConfig.getUrl() + ":" + providerConfig.getPort()),
                new EntityID(providerConfig.getUrl() + ":" + providerConfig.getPort()), new Date(), new Date(), keyService.getPublicFedJwkSet());

        //Set Authority Hints
        List<EntityID> authorityHints = new ArrayList<>();
        providerConfig.getAuthorityHints().forEach(authorityHint -> authorityHints.add(new EntityID(authorityHint)));
        entityStatementClaimsSet.setAuthorityHints(authorityHints);

        //Set Metadata
        @SuppressWarnings("deprecation")
        JSONParser parser = new JSONParser();
        metadataRepository.findAll().forEach(entry -> {
            try {
                entityStatementClaimsSet.setMetadata(new FederationMetadataType(entry.getType()), (JSONObject) parser.parse(entry.getContent()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        });

        //Standard metadata claim
        entityStatementClaimsSet.setMetadata(FederationMetadataType.FEDERATION_ENTITY, new JSONObject());

        //Sign Entity Statement with private key from key service. Check kid in key file
        EntityStatement entityStatement = null;
        try {
            entityStatement = EntityStatement.sign(entityStatementClaimsSet, keyService.getFedJwkSet().getKeyByKeyId("JMcm"));
        } catch (JOSEException e) {
            e.printStackTrace();
        }

        assert entityStatement != null;
        logger.info(entityStatement.getClaimsSet().toJSONString());
        return entityStatement.getSignedStatement().serialize();
    }
}
