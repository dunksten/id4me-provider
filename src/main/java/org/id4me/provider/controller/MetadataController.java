package org.id4me.provider.controller;


import org.id4me.provider.model.MetadataModel;
import org.id4me.provider.repository.MetadataRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class MetadataController {

    private final MetadataRepository metadataRepository;

    public MetadataController(MetadataRepository metadataRepository) {
        this.metadataRepository = metadataRepository;
    }

    @GetMapping("/")
    public String home() {
        return "redirect:/index";
    }

    @GetMapping("/index")
    public String index(Model model) {
        model.addAttribute("metadatas", metadataRepository.findAll());
        return "index";
    }

    @GetMapping(value = "/add")
    public String displayForm(MetadataModel metadata, Model model) {
        model.addAttribute("metadata", metadata);
        return "metadata";
    }

    @PostMapping(value = "/addMetadata")
    public String addMetadata(@Valid MetadataModel metadataModel, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "metadata";
        }
        metadataRepository.save(metadataModel);
        model.addAttribute("metadatas", metadataRepository.findAll());
        return "redirect:/index";
    }

    @PostMapping(value = "/updateMetadata/{id}")
    public String updateMetadata(@PathVariable("id") Long id, @Valid MetadataModel metadataModel, BindingResult result, Model model) {
        if (result.hasErrors()) {
            metadataModel.setId(id);
            return "updateMetadata";
        }
        metadataRepository.save(metadataModel);
        model.addAttribute("metadatas", metadataRepository.findAll());
        return "redirect:/index";
    }

    @GetMapping("/editMetadata/{id}")
    public String showUpdateForm(@PathVariable("id") long id, Model model) {
        MetadataModel metadataModel = metadataRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid model Id:" + id));

        model.addAttribute("metadata", metadataModel);
        return "updateMetadata";
    }

    @GetMapping("/deleteMetadata/{id}")
    public String deleteMetadata(@PathVariable("id") long id, Model model) {
        MetadataModel metadataModel = metadataRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid metadata Id:" + id));
        metadataRepository.delete(metadataModel);
        model.addAttribute("metadatas", metadataRepository.findAll());
        return "redirect:/index";
    }
}
