package org.id4me.provider.controller;

import com.nimbusds.oauth2.sdk.ParseException;
import com.nimbusds.oauth2.sdk.id.Issuer;
import com.nimbusds.openid.connect.sdk.SubjectType;
import com.nimbusds.openid.connect.sdk.op.OIDCProviderMetadata;
import org.id4me.provider.common.ProviderConfig;
import org.id4me.provider.service.KeyService;
import org.id4me.provider.service.StatementService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;

@RestController
public class WellKnownController {

    //private static final Logger logger = LoggerFactory.getLogger(WellKnownController.class);

    private final ProviderConfig providerConfig;
    private final KeyService keyService;
    private final StatementService statementService;

    public WellKnownController(ProviderConfig providerConfig, KeyService keyService, StatementService statementService) {
        this.providerConfig = providerConfig;
        this.keyService = keyService;
        this.statementService = statementService;
    }

    @GetMapping(value = "/.well-known/openid-configuration", produces = "application/json; charset=UTF-8")
    public ResponseEntity<String> openidConfigurationResponse() throws ParseException, URISyntaxException {
        //Create Provider Metadata
        OIDCProviderMetadata oidcProviderMetadata = new OIDCProviderMetadata(new Issuer(providerConfig.getUrl() + ":" + providerConfig.getPort()),
                Collections.singletonList(SubjectType.parse("public")), new URI(providerConfig.getUrl() + ":" + providerConfig.getPort() + providerConfig.getBase() + providerConfig.getJwks()));

        //Add support for verified claims
        oidcProviderMetadata.setVerifiedClaims(Collections.singletonList("name"));
        oidcProviderMetadata.setSupportsVerifiedClaims(true);

        return ResponseEntity.ok().header("Content-Type", "application/json; charset=UTF-8").body(oidcProviderMetadata.toJSONObject().toJSONString());
    }

    @GetMapping(value = "/.well-known/openid-federation", produces = "application/jose; charset=UTF-8")
    public ResponseEntity<String> openidFederationConfiguration() {

        return ResponseEntity.ok().header("Content-Type", "application/jose; charset=UTF-8").body(statementService.composeSelfSignedStatement());
    }

    @GetMapping(value = "/.well-known/jwks.json", produces = "application/json; charset=UTF-8")
    public ResponseEntity<String> jwks() {
        return ResponseEntity.ok().header("Content-Type", "application/json; charset=UTF-8").body(keyService.getPublicJwkSet().toJSONObject().toJSONString());
    }


}