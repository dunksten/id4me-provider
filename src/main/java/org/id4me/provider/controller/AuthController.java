package org.id4me.provider.controller;

import com.nimbusds.oauth2.sdk.AuthorizationCode;
import com.nimbusds.oauth2.sdk.ParseException;
import com.nimbusds.oauth2.sdk.ResponseType;
import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.http.HTTPRequest;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.id.State;
import com.nimbusds.openid.connect.sdk.AuthenticationRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;

@RestController
public class AuthController {

    //Token Endpoint
    @PostMapping(value = "token", produces = "application/json; charset=UTF-8")
    public ResponseEntity<String> tokenResponse() {

        return ResponseEntity.ok().header("Content-Type", "application/json; charset=UTF-8").body("");
    }

    @GetMapping(value = "/authorize")
    public ResponseEntity<String> authorize(HTTPRequest request) {

        String query = request.getQuery();

        AuthenticationRequest req = null;

        // Decode the query string
        try {
            req = AuthenticationRequest.parse(query);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        // Required to look up the client in the provider's database
        assert req != null;
        ClientID clientID = req.getClientID();

        // The client redirection URL, must be registered in the provider's database
        URI redirectURI = req.getRedirectionURI();

        // The response type (implies code flow)
        ResponseType rt = req.getResponseType();

        // The state, must be echoed back with the response
        State state = req.getState();

        // The requested scope
        Scope scope = req.getScope();

        // Other parameters....


        // Process the request and generate a code
        AuthorizationCode code = new AuthorizationCode();

        // Create response
        return ResponseEntity.ok().header("Content-Type", "application/json; charset=UTF-8").body("");
    }


}
