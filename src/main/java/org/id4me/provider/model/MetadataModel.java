package org.id4me.provider.model;


import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
public class MetadataModel {

    @Column(unique = true)
    @NotBlank(message = "Required")
    private String type;
    @NotBlank(message = "Required")
    private String content;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    public MetadataModel(String type, String content) {
        this.type = type;
        this.content = content;
    }

    public MetadataModel() {

    }

    public Long getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getContent() {
        return content;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
