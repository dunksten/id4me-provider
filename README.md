### Provider for ID4me Trust Framework and Verified Identity

This provider component is currently for testing purposes only.
It only functions with specific settings and the other components from the proof-of-concept.

#### Build the project

- Make sure that JDK 8 is installed.
- Open a terminal in the base directory of the project.
- Type ```./mvnw clean install -DskipTests```

#### Run the project

- Type ```./mvnw spring-boot:run```


##### JWKs in /KEYS are for testing purposes only